CREATE TABLE emprestimo (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    data_retirada DATETIME NOT NULL,
    data_limite DATETIME NOT NULL,
    data_devolucao DATETIME,
    id_cliente BIGINT(20) NOT NULL,
    FOREIGN KEY (id_cliente) REFERENCES cliente(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;