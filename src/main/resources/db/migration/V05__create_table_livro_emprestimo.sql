CREATE TABLE livro_emprestimo (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    id_livro BIGINT(20) NOT NULL,
    id_emprestimo BIGINT(20) NOT NULL,
    FOREIGN KEY (id_livro) REFERENCES livro(id),
    FOREIGN KEY (id_emprestimo) REFERENCES emprestimo(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;