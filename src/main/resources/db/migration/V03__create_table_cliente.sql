CREATE TABLE cliente (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(60) NOT NULL,
    cep VARCHAR(9) NOT NULL,
    logradouro VARCHAR(50) NOT NULL,
    bairro VARCHAR(50) NOT NULL,
    numero VARCHAR(5) NOT NULL,
    complemento TEXT NOT NULL,
    deve_emprestimo BOOLEAN NOT NULL,
    deve_multa BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

