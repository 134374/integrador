CREATE TABLE livro (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    sku VARCHAR(50) NOT NULL,
    titulo VARCHAR(200) NOT NULL,
    sinopse TEXT NOT NULL,
	disponivel BOOLEAN NOT NULL,
    id_autor BIGINT(20) NOT NULL,
    FOREIGN KEY (id_autor) REFERENCES autor(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

