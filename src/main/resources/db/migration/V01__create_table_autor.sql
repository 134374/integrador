CREATE TABLE autor (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO autor VALUES (0, 'Machado de Assis');
INSERT INTO autor VALUES (0, 'Monteiro Lobato');
INSERT INTO autor VALUES (0, 'John Ronald Reuel Tolkien');
INSERT INTO autor VALUES (0, 'George Raymond Richard Martin');