$('.js-pesquisar-clientes').on('click', function() {
	
	var clienteNome = $('.js-cliente-nome-mdl').val();
	
	$.ajax({
		url: $('#pesquisaClienteMdlForm').attr('action'),
		method: 'GET',
		contentType: 'application/JSON',
		data: {
			nome: clienteNome
		},
		success: pesquisaConcluida.bind(this)
	})
	
	
	function pesquisaConcluida(result) {
		var html = $("#tabela-pesquisa-clientes-mdl").html();
		var template = Handlebars.compile(html);
		
		$('#container-clientes-result').html(template(result));
	}

});


function selecionaCliente(id, nome) {
	$('#pesquisaClienteMdl').modal('hide'),
	$('#nomeCliente').val(nome),
	$('#idCliente').val(id);
}

	


