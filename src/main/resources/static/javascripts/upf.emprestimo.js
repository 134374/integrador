$(function () {
	
	var input = $('.js-sku-titulo-livro-input');
	var htmlTemplateAutocomplete = $('#item-autocomplete-livro').html();
	var template = Handlebars.compile(htmlTemplateAutocomplete);
	
	var emitter = $({});
	var on = emitter.on.bind(emitter);
	
	var params = {
			url: function(skuOuTitulo) {
				return '/integrador/livros/filtro?skuOuTitulo=' + skuOuTitulo;
			},
			getValue: 'titulo',
			template: {
				type: 'custom',
				method: function (titulo, livro) {
					return template(livro);
				}
			},
			list: {
				onChooseEvent: function() {
					emitter.trigger('livro-selecionado', input.getSelectedItemData()); // Dispara um evento do tipo livro-selecionado
				}
			}
	};
	
	input.easyAutocomplete(params)
	
	// ouve os eventos do tipo livro-selecionado
	on('livro-selecionado', function(e, item) {
		var response = $.ajax({
			url: 'livro',
			method: 'POST',
			data: {
				idLivro: item.id
			}
		});
		
		response.done(function(html) {
			var container = $('.js-tabela-livros-container');

			container.html(html);
		})
		
	});
	
});