package org.upf.integrador.service.session;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import org.upf.integrador.model.Livro;
import org.upf.integrador.model.LivroEmprestimo;

@SessionScope
@Service
public class TabelaLivrosEmprestimo {

	private List<LivroEmprestimo> livros = new ArrayList<LivroEmprestimo>();

	public void adicionarLivro(Livro livro) {
		boolean livroRepetido = false;
		
		for (LivroEmprestimo livroEmprestimo : livros) {
			Livro livroAdicionado = livroEmprestimo.getLivro();
			
			if (livroAdicionado.equals(livro)) {
				livroRepetido = true;
				break;
			}
		}
		
		
		if (!livroRepetido) {
			LivroEmprestimo livroEmprestimo = new LivroEmprestimo();
			livroEmprestimo.setLivro(livro);
			
			livros.add(livroEmprestimo);
		}
	}

	public void removerLivro(Livro livro) {
		LivroEmprestimo leParaRemover = null;
		
		for (LivroEmprestimo livroEmprestimo : livros) {
			if (livroEmprestimo.getLivro().equals(livro)) {
				leParaRemover = livroEmprestimo;
				break;
			}
		}
		
		if (leParaRemover != null) {
			livros.remove(leParaRemover);
		}
	}

	public List<LivroEmprestimo> getLivros() {
		return livros;
	}

	public void setLivros(List<LivroEmprestimo> livros) {
		this.livros = livros;
	}
	
	public void limparLivros() {
		this.livros = new ArrayList<LivroEmprestimo>();
	}

}
