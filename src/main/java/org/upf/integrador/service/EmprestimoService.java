package org.upf.integrador.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.upf.integrador.event.EmprestimoRealizadoEvent;
import org.upf.integrador.filter.EmprestimoFilter;
import org.upf.integrador.model.Emprestimo;
import org.upf.integrador.model.Livro;
import org.upf.integrador.model.LivroEmprestimo;
import org.upf.integrador.model.constants.EmprestimoStatus;
import org.upf.integrador.repository.EmprestimoRepository;
import org.upf.integrador.service.session.TabelaLivrosEmprestimo;

@Service
public class EmprestimoService {

	@Autowired
	private LivroService livroService;
	
	@Autowired
	private EmprestimoRepository emprestimoRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private TabelaLivrosEmprestimo tabelaLivrosEmprestimo;
	
	@Transactional
	public void salvar(Emprestimo emprestimo) {
		emprestimo.adicionarLivrosAoEmprestimo(tabelaLivrosEmprestimo.getLivros());
		
		emprestimoRepository.save(emprestimo);
		
		livroService.atualizarDisponibilidade(emprestimo, false);
		
		tabelaLivrosEmprestimo.limparLivros();
		
		publisher.publishEvent(new EmprestimoRealizadoEvent(emprestimo));
	}
	
	public Emprestimo buscarPeloId(Long id) {
		return emprestimoRepository.findOne(id);
	}

	public List<Emprestimo> filtrar(EmprestimoFilter emprestimoFilter) {
		return emprestimoRepository.findAll();
	}

	public TabelaLivrosEmprestimo adicionarLivro(Long idLivro) {
		Livro livro = livroService.buscarPeloId(idLivro);
		tabelaLivrosEmprestimo.adicionarLivro(livro);
		return tabelaLivrosEmprestimo;
	}

	public TabelaLivrosEmprestimo removerLivro(Long idLivro) {
		Livro livro = livroService.buscarPeloId(idLivro);
		tabelaLivrosEmprestimo.removerLivro(livro);
		return tabelaLivrosEmprestimo;
	}

	public List<Emprestimo> buscaEmprestimosAtrasados() {
		List<Emprestimo> emprestimos = emprestimoRepository.findAll();
		
		List<Emprestimo> atrasados = new ArrayList<>();
		
		for (Emprestimo emprestimo : emprestimos) {
			if (emprestimo.getEmprestimoStatus().equals(EmprestimoStatus.ATRASADO)) {
				atrasados.add(emprestimo);
			}
		}
		
		return atrasados;
	}

	public void efetuarDevolucao(Long id) {
		Emprestimo emprestimo = buscarPeloId(id);
		
		List<LivroEmprestimo> livrosEmprestados = emprestimo.getLivros();
		
		for (LivroEmprestimo livroEmprestimo : livrosEmprestados) {
			Livro livro = livroService.buscarPeloId(livroEmprestimo.getLivro().getId());
			livro.setDisponivel(true);
			livroService.salvar(livro);
		}
		
		emprestimo.setDataDevolucao(LocalDate.now());
		emprestimoRepository.saveAndFlush(emprestimo);
	}
}
