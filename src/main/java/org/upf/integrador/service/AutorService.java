package org.upf.integrador.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.upf.integrador.filter.AutorFilter;
import org.upf.integrador.model.Autor;
import org.upf.integrador.repository.AutorRepository;

@Service
public class AutorService {

	@Autowired
	private AutorRepository autorRepository;
	
	@Transactional
	public void salvar(Autor autor) {
		autorRepository.saveAndFlush(autor);
	}
	
	public List<Autor> buscarTodos() {
		return autorRepository.findAll();
	}
	
	public List<Autor> filtrar(AutorFilter filter) {
		return autorRepository.filtrar(filter);
	}

	public Autor buscarPeloId(Long id) {
		return autorRepository.findOne(id);
	}

	public void deletarPeloId(Long id) {
		autorRepository.delete(buscarPeloId(id));
	}
	
}
