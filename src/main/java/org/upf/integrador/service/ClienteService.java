package org.upf.integrador.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.upf.integrador.filter.ClienteFilter;
import org.upf.integrador.model.Cliente;
import org.upf.integrador.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public List<Cliente> filtrar(ClienteFilter filter) {
		return clienteRepository.filtrar(filter);
	}

	public void salvar(Cliente cliente) {
		clienteRepository.save(cliente);
	}

	public Cliente buscarPeloId(Long id) {
		return clienteRepository.findOne(id);
	}

	public void deletarPeloId(Long id) {
		clienteRepository.delete(buscarPeloId(id));
	}

	public List<Cliente> buscarTodos() {
		return clienteRepository.findAll();
	}

	public List<Cliente> buscarPeloNome(String nome) {
		return clienteRepository.findByNomeStartingWithIgnoreCase(nome);
	}
	
}
