package org.upf.integrador.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.upf.integrador.dto.LivroDTO;
import org.upf.integrador.filter.LivroFilter;
import org.upf.integrador.model.Emprestimo;
import org.upf.integrador.model.Livro;
import org.upf.integrador.model.LivroEmprestimo;
import org.upf.integrador.repository.LivroRepository;

@Service
public class LivroService {

	@Autowired
	private LivroRepository livroRepository;

	@Transactional
	public void salvar(Livro livro) {
		livroRepository.save(livro);
	}
	
	public void atualizarDisponibilidade(Emprestimo emprestimo, boolean novoStatus) {
		for (LivroEmprestimo livroEmprestado : emprestimo.getLivros()) {
			Long livroId = livroEmprestado.getLivro().getId();
			
			Livro livro = buscarPeloId(livroId);
			livro.setDisponivel(novoStatus);
			salvar(livro);
		}
	}
	
	public List<Livro> buscarTodos() {
		return livroRepository.findAll();
	}

	public List<Livro> filtrar(LivroFilter filtro) {
		return livroRepository.filtrar(filtro);
	}

	public Livro buscarPeloId(Long id) {
		return livroRepository.findOne(id);
	}

	public void deletarPeloId(Long id) {
		livroRepository.delete(buscarPeloId(id));
	}

	public List<Livro> buscarDisponiveis() {
		return livroRepository.findByDisponivel(true);
	}

	public List<LivroDTO> buscarPorSkuOuTitulo(String skuOuTitulo) {
		List<LivroDTO> livros = livroRepository.buscarPorSkuOuTitulo(skuOuTitulo);
		List<LivroDTO> livrosDisponiveis = new ArrayList<>();
		
		for (LivroDTO livroDTO : livros) {
			if (livroDTO.isDisponivel()) {
				livrosDisponiveis.add(livroDTO);
			}
		}
		
		return livrosDisponiveis;
	}
	
}
