package org.upf.integrador.model.constants;

public enum PenalidadeStatus {

	FECHADA("Fechada"),
	ABERTA("Aberta");

	private String descricao;

	private PenalidadeStatus(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
