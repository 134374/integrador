package org.upf.integrador.model.constants;

public enum EmprestimoStatus {

	ATIVO("Ativo"),
	ATRASADO("Atrasado"),
	FINALIZADO("Finalizado"), 
	NOVO("Novo");
	
	private String descricao;
	
	EmprestimoStatus(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() { 
		return descricao;
	}
	
}
