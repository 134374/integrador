package org.upf.integrador.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.upf.integrador.model.constants.EmprestimoStatus;
import org.upf.integrador.service.session.TabelaLivrosEmprestimo;

@Entity
@Table(name = "emprestimo")
public class Emprestimo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_cliente")
	@NotNull(message = "É necessário ao menos um cliente para gerar um empréstimo")
	private Cliente cliente;

	@OneToMany(mappedBy = "emprestimo", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<LivroEmprestimo> livros;

	@Column(name = "data_retirada")
	private LocalDate dataRetirada = LocalDate.now();

	@Column(name = "data_limite")
	@NotNull(message = "É necessário selecionar uma data limite para a devolução")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataLimite;

	@Column(name = "data_devolucao")
	private LocalDate dataDevolucao = null;

	@Transient
	private TabelaLivrosEmprestimo tabelaLivrosEmprestimo;
	
	@Transient
	public void adicionarLivrosAoEmprestimo(List<LivroEmprestimo> le) {
		this.livros = le;
		
		for (LivroEmprestimo livroEmprestimo : livros) {
			livroEmprestimo.setEmprestimo(this);
		}
	}
	
	@Transient
	public EmprestimoStatus getEmprestimoStatus() {
		LocalDate agora = LocalDate.now();
		
		if (dataLimite == null) {
			return EmprestimoStatus.NOVO;
		} else if (agora.isBefore(dataLimite) && dataDevolucao == null) {
			return EmprestimoStatus.ATIVO;
		} else if (agora.isAfter(dataLimite) && dataDevolucao == null) {
			return EmprestimoStatus.ATRASADO;
		} else if (dataDevolucao != null) {
			return EmprestimoStatus.FINALIZADO;
		} else {
			return EmprestimoStatus.FINALIZADO;
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<LivroEmprestimo> getLivros() {
		return livros;
	}

	public void setLivros(List<LivroEmprestimo> livros) {
		this.livros = livros;
	}

	public LocalDate getDataRetirada() {
		return dataRetirada;
	}

	public void setDataRetirada(LocalDate dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	public LocalDate getDataLimite() {
		return dataLimite;
	}

	public void setDataLimite(LocalDate dataLimite) {
		this.dataLimite = dataLimite;
	}

	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(LocalDate dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public TabelaLivrosEmprestimo getTabelaLivrosEmprestimo() {
		return tabelaLivrosEmprestimo;
	}

	public void setTabelaLivrosEmprestimo(TabelaLivrosEmprestimo tabelaLivrosEmprestimo) {
		this.tabelaLivrosEmprestimo = tabelaLivrosEmprestimo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Emprestimo other = (Emprestimo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}