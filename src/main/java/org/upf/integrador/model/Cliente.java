package org.upf.integrador.model;

import java.beans.Transient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Nome é obrigatório")
	private String nome;

	@NotBlank(message = "Por favor, informe o logradouro")
	private String logradouro;

	@NotBlank(message = "Por favor, informe o número")
	private String numero;

	private String complemento;

	@NotBlank(message = "Por favor, informe o bairro")
	private String bairro;

	@NotBlank(message = "Por favor, informe o cep")
	private String cep;

	@NotNull
	@Column(name = "deve_emprestimo")
	private Boolean deveEmprestimo = false;

	@NotNull
	@Column(name = "deve_multa")
	private Boolean deveMulta = false;

	@JsonIgnore
	@Transient
	private Boolean podeRetirar() {
		return !deveEmprestimo && !deveMulta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Boolean getDeveEmprestimo() {
		return deveEmprestimo;
	}

	public void setDeveEmprestimo(Boolean deveEmprestimo) {
		this.deveEmprestimo = deveEmprestimo;
	}

	public Boolean getDeveMulta() {
		return deveMulta;
	}

	public void setDeveMulta(Boolean deveMulta) {
		this.deveMulta = deveMulta;
	}

}
