package org.upf.integrador.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "org.upf.integrador.service", "org.upf.integrador.scheduled" })
public class ServiceConfig {

}
