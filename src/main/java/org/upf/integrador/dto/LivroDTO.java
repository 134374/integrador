package org.upf.integrador.dto;

public class LivroDTO {

	private Long id;

	private String sku;

	private String titulo;
	
	private boolean disponivel;

	public LivroDTO(Long id, String sku, String titulo, boolean disponivel) {
		super();
		this.id = id;
		this.sku = sku;
		this.titulo = titulo;
		this.disponivel = disponivel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}

}
