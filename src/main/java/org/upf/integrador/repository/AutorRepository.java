package org.upf.integrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.upf.integrador.model.Autor;
import org.upf.integrador.repository.helper.AutorRepositoryQueries;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long>, AutorRepositoryQueries {

}
