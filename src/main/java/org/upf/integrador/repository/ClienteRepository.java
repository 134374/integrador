package org.upf.integrador.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.upf.integrador.model.Cliente;
import org.upf.integrador.repository.helper.ClienteRepositoryQueries;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>, ClienteRepositoryQueries {

	public List<Cliente> findByNomeStartingWithIgnoreCase(String nome);
	
}
