package org.upf.integrador.repository.helper;

import java.util.List;

import org.upf.integrador.filter.ClienteFilter;
import org.upf.integrador.model.Cliente;

public interface ClienteRepositoryQueries {

	public List<Cliente> filtrar(ClienteFilter filter);
	
}
