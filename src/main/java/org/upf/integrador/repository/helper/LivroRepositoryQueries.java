package org.upf.integrador.repository.helper;

import java.util.List;

import org.upf.integrador.dto.LivroDTO;
import org.upf.integrador.filter.LivroFilter;
import org.upf.integrador.model.Livro;

public interface LivroRepositoryQueries {

	public List<Livro> filtrar(LivroFilter filtro);
	
	public List<LivroDTO> buscarPorSkuOuTitulo(String skuOuTitulo);
	
}
