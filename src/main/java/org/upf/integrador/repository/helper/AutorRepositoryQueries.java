package org.upf.integrador.repository.helper;

import java.util.List;

import org.upf.integrador.filter.AutorFilter;
import org.upf.integrador.model.Autor;

public interface AutorRepositoryQueries {

	public List<Autor> filtrar(AutorFilter filtro);
	
}
