package org.upf.integrador.repository.helper;

import static org.springframework.util.StringUtils.isEmpty;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.upf.integrador.filter.AutorFilter;
import org.upf.integrador.model.Autor;

public class AutorRepositoryImpl implements AutorRepositoryQueries {

	@PersistenceContext
	private EntityManager manager; // Injeta o Entity Manager
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Autor> filtrar(AutorFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Autor.class); // Cria uma criteria em cima da sessão do hibernate.
		
		if (filtro != null) {
			if (!isEmpty(filtro.getNome())) {
				criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
			}
		}
		
		
		return criteria.list();
	}

}
