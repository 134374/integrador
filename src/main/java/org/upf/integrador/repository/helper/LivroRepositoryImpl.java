package org.upf.integrador.repository.helper;

import static org.springframework.util.StringUtils.isEmpty;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.upf.integrador.dto.LivroDTO;
import org.upf.integrador.filter.LivroFilter;
import org.upf.integrador.model.Autor;
import org.upf.integrador.model.Livro;

public class LivroRepositoryImpl implements LivroRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Livro> filtrar(LivroFilter filtro) {
		Criteria livroCriteria = manager.unwrap(Session.class).createCriteria(Livro.class);
		
		if (filtro != null) {
			if (!isEmpty(filtro.getSku())) {
				livroCriteria.add(Restrictions.ilike("sku", filtro.getSku(), MatchMode.ANYWHERE));
			}
			
			if (!isEmpty(filtro.getTitulo())) {
				livroCriteria.add(Restrictions.ilike("titulo", filtro.getTitulo(), MatchMode.ANYWHERE));
			}
			
			// Nome do autor
			if (!isEmpty(filtro.getAutor())) {
				Criteria autorCriteria = manager.unwrap(Session.class).createCriteria(Autor.class);
				
				List<Autor> autores = autorCriteria.add(Restrictions.ilike("nome", filtro.getAutor(), MatchMode.ANYWHERE)).list();
				
				for (Autor autor : autores) {
					livroCriteria.add(Restrictions.eq("autor", autor));
				}
			}
		}
		
		return livroCriteria.list();
	}

	@Override
	public List<LivroDTO> buscarPorSkuOuTitulo(String param) {
		String jpql = "SELECT new org.upf.integrador.dto.LivroDTO(id, sku, titulo, disponivel) FROM Livro WHERE sku LIKE :param OR titulo LIKE :param AND disponivel = true";
		
		return manager.createQuery(jpql, LivroDTO.class)
				.setParameter("param", param.toLowerCase() + "%")
				.getResultList();
	}
	
}
