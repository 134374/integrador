package org.upf.integrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.upf.integrador.model.Livro;
import org.upf.integrador.repository.helper.LivroRepositoryQueries;
import java.lang.Boolean;
import java.util.List;

@Repository
public interface LivroRepository extends JpaRepository<Livro, Long>, LivroRepositoryQueries {
	
	List<Livro> findByDisponivel(Boolean disponivel);
	
}
