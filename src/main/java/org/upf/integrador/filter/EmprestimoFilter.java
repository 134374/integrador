package org.upf.integrador.filter;

import java.time.LocalDate;

import org.upf.integrador.model.constants.EmprestimoStatus;

public class EmprestimoFilter {

	private LocalDate dataDe;

	private LocalDate dataAte;

	private EmprestimoStatus status;

	private String cliente;

	private String livro;

	public LocalDate getDataDe() {
		return dataDe;
	}

	public void setDataDe(LocalDate dataDe) {
		this.dataDe = dataDe;
	}

	public LocalDate getDataAte() {
		return dataAte;
	}

	public void setDataAte(LocalDate dataAte) {
		this.dataAte = dataAte;
	}

	public EmprestimoStatus getStatus() {
		return status;
	}

	public void setStatus(EmprestimoStatus status) {
		this.status = status;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getLivro() {
		return livro;
	}

	public void setLivro(String livro) {
		this.livro = livro;
	}

}
