package org.upf.integrador.event.listener;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.upf.integrador.event.EmprestimoRealizadoEvent;

@Component
public class EmprestimoListener {

	@EventListener
	public void emprestimoRealizado(EmprestimoRealizadoEvent event) {
		System.out.println("Emprestimo realizado");
	}
	
}
