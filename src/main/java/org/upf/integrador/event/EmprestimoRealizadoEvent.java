package org.upf.integrador.event;

import org.upf.integrador.model.Emprestimo;

public class EmprestimoRealizadoEvent {

	private Emprestimo emprestimo;
	
	public EmprestimoRealizadoEvent(Emprestimo emprestimo) {
		this.emprestimo = emprestimo;
	}
	
	public Emprestimo getEmprestimo() {
		return emprestimo;

	}
	
}
