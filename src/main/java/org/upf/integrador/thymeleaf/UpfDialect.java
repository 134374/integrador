package org.upf.integrador.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;
import org.upf.integrador.thymeleaf.processor.ClassForErrorAttributeTagProcessor;
import org.upf.integrador.thymeleaf.processor.MessageElementTagProcessor;

public class UpfDialect extends AbstractProcessorDialect {

	public UpfDialect() {
		super("Projeto Integrador", "upf", StandardDialect.PROCESSOR_PRECEDENCE);
	}
	
	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processors = new HashSet<>();
		processors.add(new ClassForErrorAttributeTagProcessor(dialectPrefix));
		processors.add(new MessageElementTagProcessor(dialectPrefix));
		
		return processors;
	}

}
