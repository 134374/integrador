package org.upf.integrador.thymeleaf.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class MessageElementTagProcessor extends AbstractElementTagProcessor {

	private static final String ATTRIBUTE = "message";
	private static final Integer PRECEDENCE = 1000;
	
	public MessageElementTagProcessor(String dialectPrefix) {
		super(TemplateMode.HTML, dialectPrefix, ATTRIBUTE, true, null, false, PRECEDENCE);
	}

	@Override
	protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {
		IModelFactory factory = context.getModelFactory();
		
		IModel model = factory.createModel();
		
		model.add(factory.createStandaloneElementTag("th:block", "th:replace", "fragments/MensagemSucesso :: alert"));
		model.add(factory.createStandaloneElementTag("th:block", "th:replace", "fragments/MensagemAviso :: alert"));
		model.add(factory.createStandaloneElementTag("th:block", "th:replace", "fragments/MensagemErro :: alert"));
		
		structureHandler.replaceWith(model, true);
	}

}
