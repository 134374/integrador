package org.upf.integrador.thymeleaf.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring4.util.FieldUtils;
import org.thymeleaf.templatemode.TemplateMode;

public class ClassForErrorAttributeTagProcessor extends AbstractAttributeTagProcessor {

	private static final String ATTRIBUTE = "classforerror";
	private static final Integer PRECEDENCE = 1000;

	public ClassForErrorAttributeTagProcessor(String dialectPrefix) {
		super(TemplateMode.HTML, dialectPrefix, null, false, ATTRIBUTE, true, PRECEDENCE, true);
	}

	@Override
	protected void doProcess(ITemplateContext context, IProcessableElementTag tag, AttributeName attributeName,	String attributeValue, IElementTagStructureHandler structureHandler) {

		boolean fieldHasError = FieldUtils.hasErrors(context, attributeValue);

		if (fieldHasError) {
			String existingClasses = tag.getAttributeValue("class");
			structureHandler.setAttribute("class", existingClasses + " has-error");
		}
	}

}
