package org.upf.integrador.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.upf.integrador.filter.ClienteFilter;
import org.upf.integrador.model.Cliente;
import org.upf.integrador.service.ClienteService;

@Controller
@RequestMapping("/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/novo")
	public ModelAndView novo(Cliente cliente) {
		return new ModelAndView("cliente/CadastroCliente");
	}
	
	@PostMapping("/novo")
	public ModelAndView cadastrar(@Valid Cliente cliente, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(cliente);
		}
		
		clienteService.salvar(cliente);
		attributes.addFlashAttribute("mensagem", "Cliente cadastrado com sucesso!");
		return new ModelAndView("redirect:/clientes/novo");
	}
	
	// Pesquisa
	
	@GetMapping
	public ModelAndView pesquisar(ClienteFilter filter) {
		ModelAndView mv = new ModelAndView("cliente/PesquisaCliente");
		mv.addObject("clientes", clienteService.filtrar(filter));
		return mv;
	}
	
	@RequestMapping(method = RequestMethod.GET ,consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<Cliente> pesquisar(String nome) {
		return clienteService.buscarPeloNome(nome);
	}
	
	
	// Edição
	
	@GetMapping("/{id}")
	public ModelAndView editar(@PathVariable Long id) {
		Cliente cliente = clienteService.buscarPeloId(id);
		ModelAndView mv = novo(cliente);
		mv.addObject(cliente);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView atualizar(@Valid Cliente cliente, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(cliente);
		}

		clienteService.salvar(cliente);
		attributes.addFlashAttribute("mensagem", "Cliente atualizado com sucesso!");
		return new ModelAndView("redirect:/autores/novo");
	}
	
	// Remoção
	@GetMapping("/excluir/{id}")
	public ModelAndView remover(@PathVariable Long id, RedirectAttributes attributes) {
		try {
			clienteService.deletarPeloId(id);
		} catch (DataIntegrityViolationException e) {
			attributes.addFlashAttribute("aviso", "Não é possível excluir um cliente que possua empréstimos cadastrados");
		}
		
		return new ModelAndView("redirect:/clientes");
	}
}
