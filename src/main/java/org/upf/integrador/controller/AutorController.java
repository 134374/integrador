package org.upf.integrador.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.upf.integrador.filter.AutorFilter;
import org.upf.integrador.model.Autor;
import org.upf.integrador.service.AutorService;

@Controller
@RequestMapping("/autores")
public class AutorController {

	@Autowired
	private AutorService autorService;
	
	// Cadastro
	
	@GetMapping("/novo")
	public ModelAndView initCadastro(Autor autor) {
		return new ModelAndView("autor/CadastroAutor");
	}
	
	@PostMapping("/novo")
	public ModelAndView processCadastro(@Valid Autor autor, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return initCadastro(autor);
		}

		autorService.salvar(autor);
		attributes.addFlashAttribute("mensagem", "Autor cadastrado com sucesso!");
		return new ModelAndView("redirect:/autores/novo");
	}
	
	// Pesquisa
	@GetMapping
	public ModelAndView pesquisar(AutorFilter autorFilter) {
		ModelAndView mv = new ModelAndView("autor/PesquisaAutor");
		mv.addObject("autores", autorService.filtrar(autorFilter));
		return mv;
	}
	
	// Edição
	@GetMapping("/{id}")
	public ModelAndView initEdit(@PathVariable Long id) {
		Autor autor = autorService.buscarPeloId(id);
		ModelAndView mv = initCadastro(autor);
		mv.addObject(autor);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView processEdit(@Valid Autor autor, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return initCadastro(autor);
		}

		autorService.salvar(autor);
		attributes.addFlashAttribute("mensagem", "Autor atualizado com sucesso!");
		return new ModelAndView("redirect:/autores/novo");
	}
	
	// Remoção
	@GetMapping("/excluir/{id}")
	public ModelAndView remover(@PathVariable Long id, RedirectAttributes attributes) {
		try {
			autorService.deletarPeloId(id);
		} catch (DataIntegrityViolationException e) {
			attributes.addFlashAttribute("aviso", "Não é possível excluir um autor que possua livros cadastrados.");
		}
		
		return new ModelAndView("redirect:/autores");
	}
}
