package org.upf.integrador.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.upf.integrador.dto.LivroDTO;
import org.upf.integrador.filter.LivroFilter;
import org.upf.integrador.model.Livro;
import org.upf.integrador.service.AutorService;
import org.upf.integrador.service.LivroService;

@Controller
@RequestMapping("/livros")
public class LivroController {

	@Autowired
	private AutorService autorService;
	
	@Autowired
	private LivroService livroService;
	
	// Cadastro
	
	@GetMapping("/novo")
	public ModelAndView novo(Livro livro) {
		ModelAndView mv = new ModelAndView("livro/CadastroLivro");
		mv.addObject("autores", autorService.buscarTodos());
		return mv;
	}
	
	@PostMapping("/novo")
	public ModelAndView cadastrar(@Valid Livro livro, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(livro);
		}
		
		livroService.salvar(livro);
		attributes.addFlashAttribute("mensagem", "Livro cadastrado com sucesso!");
		return new ModelAndView("redirect:/livros/novo");
	}
	
	// Pesquisa
	@GetMapping
	public ModelAndView pesquisar(LivroFilter filter) {
		ModelAndView mv = new ModelAndView("livro/PesquisaLivro");
		mv.addObject("livros", livroService.filtrar(filter));
		return mv;
	}
	
	// Filtro e autocomplete tela de emprestimo
	@GetMapping("/filtro")
	public @ResponseBody List<LivroDTO> pesquisar(String skuOuTitulo) {
		return livroService.buscarPorSkuOuTitulo(skuOuTitulo);
	}
	
	
	// Edição
	@GetMapping("/{id}")
	public ModelAndView editar(@PathVariable Long id) {
		Livro livro = livroService.buscarPeloId(id);
		ModelAndView mv = novo(livro);
		mv.addObject(livro);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView atualizar(@Valid Livro livro, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(livro);
		}

		livroService.salvar(livro);
		attributes.addFlashAttribute("mensagem", "Livro atualizado com sucesso!");
		return new ModelAndView("redirect:/livros/novo");
	}
	
	// Remoção
	@GetMapping("/excluir/{id}")
	public ModelAndView remover(@PathVariable Long id, RedirectAttributes attributes) {
		try {
			livroService.deletarPeloId(id);
		} catch (DataIntegrityViolationException e) {
			attributes.addFlashAttribute("aviso", "Não é possível excluir um livro que possua emprestimos.");
		}

		return new ModelAndView("redirect:/livros");
	}
}
