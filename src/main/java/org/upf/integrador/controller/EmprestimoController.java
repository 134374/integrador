package org.upf.integrador.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.upf.integrador.filter.EmprestimoFilter;
import org.upf.integrador.model.Emprestimo;
import org.upf.integrador.service.ClienteService;
import org.upf.integrador.service.EmprestimoService;
import org.upf.integrador.service.LivroService;
import org.upf.integrador.service.session.TabelaLivrosEmprestimo;

@Controller
@RequestMapping("/emprestimos")
public class EmprestimoController {

	@Autowired
	private EmprestimoService emprestimoService;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private LivroService livroService;
	
	// Cadastro
	
	@GetMapping("/novo")
	public ModelAndView initCadastro(Emprestimo emprestimo) {
		ModelAndView mv = new ModelAndView("emprestimo/CadastroEmprestimo");
		mv.addObject("clientes", clienteService.buscarTodos());
		mv.addObject("livros", livroService.buscarDisponiveis());
		
		emprestimo.setTabelaLivrosEmprestimo(new TabelaLivrosEmprestimo());
		
		return mv;
	}

	@PostMapping("/novo")
	public ModelAndView processCadastro(@Valid Emprestimo emprestimo, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return initCadastro(emprestimo);
		}

		emprestimoService.salvar(emprestimo);
		
		attributes.addFlashAttribute("mensagem", "Emprestimo efetuado com sucesso!");
		return new ModelAndView("redirect:/emprestimos/novo");
	}

	// Pesquisa
	@GetMapping
	public ModelAndView pesquisar(EmprestimoFilter emprestimoFilter) {
		ModelAndView mv = new ModelAndView("emprestimo/PesquisaEmprestimo");
		mv.addObject("emprestimos", emprestimoService.filtrar(emprestimoFilter));
		return mv;
	}
	

	/**
	 * Funcionalidade de adicionar livro ao emprestimo de sessão
	 * 
	 * @param idLivro
	 * @return
	 */
	@PostMapping("/livro")
	public ModelAndView adicionarLivro(Long idLivro) {
		TabelaLivrosEmprestimo tabelaLivrosEmprestimo = emprestimoService.adicionarLivro(idLivro);
		
		ModelAndView mv = new ModelAndView("emprestimo/TabelaLivrosEmprestimo");
		mv.addObject("livroEmprestimoList", tabelaLivrosEmprestimo.getLivros());
		
		return mv;
	}
	
	@PostMapping("/removerLivro")
	public ModelAndView removerLivro(Long idLivro) {
		TabelaLivrosEmprestimo tabelaLivrosEmprestimo = emprestimoService.removerLivro(idLivro);
		
		ModelAndView mv = new ModelAndView("emprestimo/TabelaLivrosEmprestimo");
		mv.addObject("livroEmprestimoList", tabelaLivrosEmprestimo.getLivros());
		
		return mv;
	}
	
	@GetMapping("/{id}")
	public ModelAndView efetuarDevolucao(@PathVariable Long id) {
		emprestimoService.efetuarDevolucao(id);
		return new ModelAndView("redirect:/emprestimos");
	}
	
	@GetMapping("/expandir/{id}")
	public ModelAndView visualizarEmprestimo(@PathVariable Long id) {
		Emprestimo emprestimo = emprestimoService.buscarPeloId(id);
		ModelAndView mv = new ModelAndView("emprestimo/DetalhesEmprestimo");
		mv.addObject("livroEmprestimoList", emprestimo.getLivros());
		mv.addObject(emprestimo);
		return mv;
	}
}
