package org.upf.integrador.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;
import org.upf.integrador.model.Cliente;

public class ClienteConverter implements Converter<String, Cliente> {

	@Override
	public Cliente convert(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		
		Cliente cliente = new Cliente();
		cliente.setId(Long.valueOf(id));
		return cliente;
	}

}
