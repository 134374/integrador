package org.upf.integrador.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;
import org.upf.integrador.model.Autor;

public class AutorConverter implements Converter<String, Autor> {

	@Override
	public Autor convert(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		
		Autor autor = new Autor();
		autor.setId(Long.valueOf(id));
		return autor;
	}

}
